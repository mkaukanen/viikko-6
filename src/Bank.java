
import java.util.ArrayList;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class Bank {
ArrayList<debitAccount> accountRegister = new ArrayList<>();
ArrayList<creditAccount> accountRegister2 = new ArrayList<>();
debitAccount db;
creditAccount cd;
    
public void addAccount(String s, float f){
    db = new debitAccount(s);
    accountRegister.add(db);
    db.addMoney(db, f);
    System.out.println("Tili luotu.");    
}
public void addAccount(String s, float f, float f2){
    cd = new creditAccount(s, f2);
    accountRegister2.add(cd);
    cd.addMoney(cd, f);
    System.out.println("Tili luotu.");   
}
public void addMoney(String s, float f){
    for (debitAccount a : accountRegister){
        if (a.accountID.equals(s)){
            debitAccount s2 =a;
            db.addMoney(s2 ,f);
        } 
    for (creditAccount b: accountRegister2){
        if (b.accountID.equals(s)){
            creditAccount s3 =b;
            cd.addMoney(s3, f);
        }
    }
    }
}

public void drawMoney(String s, float f){
        for (debitAccount a : accountRegister){
        if (a.accountID.equals(s)){
            debitAccount s2 =a;
            db.drawMoney(s2 ,f);
        }
        }
        for (creditAccount b: accountRegister2){
        if (b.accountID.equals(s)){
            creditAccount s3 =b;
            cd.drawMoney(s3, f);
        }
        }  
}
 
public void getAccount(String s){
        for (debitAccount a : accountRegister){
        if (a.accountID.equals(s)){
            debitAccount s2 =a;
            float balance = db.getAccountBalance(s2);
            System.out.println("Tilinumero: "+s+" Tilillä rahaa: "+ Math.round(balance));
        }  //Math.round used above to format the printing output into integer, as the 
           //exercise was simplified not to take floats, however I didn't wish to make
           //it that unpractical that you couldn't deposit float amounts.
        }
        for (creditAccount b : accountRegister2){
        if (b.accountID.equals(s)){
            creditAccount s3 =b;
            float balance = cd.getAccountBalance(s3);
            System.out.println("Tilinumero: "+s+" Tilillä rahaa: "+ Math.round(balance));
        }
        }
}

public void getAccounts(){
    System.out.println("Kaikki tilit:");
    for (debitAccount a : accountRegister){
        System.out.println("Tilinumero: "+ a.accountID + " Tilillä rahaa: " +Math.round(a.balance));
    }
    for (creditAccount b : accountRegister2){
        System.out.println("Tilinumero: "+b.accountID+" Tilillä rahaa: "+Math.round(b.balance)+" Luottoraja: "+Math.round(b.creditLimit));
    }
} 
public void removeAccount(String s){
    for (debitAccount a : accountRegister){
        if (a.accountID.equals(s)){
            accountRegister.remove(a);
            System.out.println("Tili poistettu.");
        }
    }
    for (creditAccount b : accountRegister2){
        if (b.accountID.equals(s)){
            accountRegister.remove(b);  
            System.out.println("Tili poistettu.");
        }   
}   //exception handling wasn't required.
}
}
