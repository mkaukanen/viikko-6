
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    // D = debit account method / variable, C = credit account method / variable
    int choice;
    Bank bank = new Bank();
    do{
    System.out.print("\n");
    System.out.println("*** PANKKIJÄRJESTELMÄ ***");
    System.out.println("1) Lisää tavallinen tili");
    System.out.println("2) Lisää luotollinen tili");
    System.out.println("3) Tallenna tilille rahaa");
    System.out.println("4) Nosta tililtä");
    System.out.println("5) Poista tili");
    System.out.println("6) Tulosta tili");
    System.out.println("7) Tulosta kaikki tilit");
    System.out.println("0) Lopeta");
    Scanner scan = new Scanner(System.in);
    System.out.print("Valintasi: ");
    choice = Integer.parseInt(scan.next());
    if (choice == 1) {
        System.out.print("Syötä tilinumero: ");
        String DaccountID = scan.next();
        System.out.print("Syötä rahamäärä: ");
        float Dstartsum = Float.parseFloat(scan.next());
        bank.addAccount(DaccountID, Dstartsum);
    }
    else if (choice == 2) {
        System.out.print("Syötä tilinumero: ");
        String CaccountID = scan.next();
        System.out.print("Syötä rahamäärä: ");
        float Cstartsum = Float.parseFloat(scan.next());
        System.out.print("Syötä luottoraja: ");
        float creditLimit =Float.parseFloat(scan.next());
        bank.addAccount(CaccountID, Cstartsum, creditLimit);
    }
    else if (choice == 3) {
        System.out.print("Syötä tilinumero: ");
        String accountID = scan.next();
        System.out.print("Syötä rahamäärä: ");
        float addsum = Float.parseFloat(scan.next());
        bank.addMoney(accountID, addsum);
    }
    else if (choice == 4) {
        System.out.print("Syötä tilinumero: ");
        String accountID = scan.next();
        System.out.print("Syötä rahamäärä: ");
        float outsum = Float.parseFloat(scan.next());
        bank.drawMoney(accountID, outsum);
    }
    else if (choice == 5) {
        System.out.print("Syötä poistettava tilinumero: ");
        String accountID = scan.next();
        bank.removeAccount(accountID);
    }
    else if (choice == 6) {
        System.out.print("Syötä tulostettava tilinumero: ");
        String accountID = scan.next();
        bank.getAccount(accountID);
    }
    else if (choice == 7) {
        bank.getAccounts();
    }
    else if (choice != 0) {
        System.out.println("Valinta ei kelpaa.");
    }   
    }while (choice !=0);
    }
}
