/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
abstract public class Account {
    protected float balance;
    protected String accountID;
    
    public void addMoney(debitAccount s, float f){
        s.balance = s.balance + f;
        }
    public void addMoney(creditAccount s, float f){
        s.balance = s.balance + f;
        }
    
    public void drawMoney(debitAccount s, float f){
        if (s.balance <= 0){
            System.out.println("Tili on tyhjä!");
        }
        else{
        s.balance = s.balance - f; 
        }
        }
    
    public float getAccountBalance(debitAccount s){
        return s.balance;  
        }
        
    public float getAccountBalance(creditAccount s){
        return s.balance;  
        }
    
}   class debitAccount extends Account{
    
    public debitAccount(String s){
        balance = 0;
        accountID = s;
    }
}

    class creditAccount extends Account{
        float creditLimit;
        
        public creditAccount(String s, float f2){
            balance = 0;
            accountID = s;
            creditLimit = f2;
        }
        
        public void drawMoney(creditAccount s, float f){
        if (s.balance <= -s.creditLimit){
            System.out.println("Luotto on täynnä!");
        }
        else{
        s.balance = s.balance - f; 
        }
        }
    }

